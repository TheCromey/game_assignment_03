﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIManager : MonoBehaviour {

    public GameObject pressE;
    public GameObject pressQ;
    public GameObject capStanUI;
    public GameObject helmUI;
    public GameObject cannonUI;
    public GameObject plankUI;



    PlayerController player;

    // Use this for initialization
    void Start () {
        player = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (player.isSteeringWheel || player.isCannon || player.isAnchor || player.isBoardingPlank)
        {
            pressE.SetActive(true);
        } else
            pressE.SetActive(false);

        if (player.drivingShip || player.onCannon)
        {
            pressQ.SetActive(true);
        }else
            pressQ.SetActive(false);

        if (player.isAnchor)
        {
            capStanUI.SetActive(true);
        } else
            capStanUI.SetActive(false);

        if (player.isCannon)
        {
            cannonUI.SetActive(true);
        } else
            cannonUI.SetActive(false);

        if (player.isSteeringWheel)
        {
            helmUI.SetActive(true);
        } else
            helmUI.SetActive(false);

        if (player.isBoardingPlank)
        {
            plankUI.SetActive(true);
        } else
            plankUI.SetActive(false);
    }
}
