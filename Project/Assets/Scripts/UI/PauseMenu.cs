﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

    public GameObject menuUI;
    public GameObject optionsUI;
    [SerializeField]
    public static bool isPaused = false;

	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Pause();
            }
            else if (isPaused)
            {
                Resume();
            }
        }

	}
    //This locks the cursor and disables the pause UI
    public void Resume()
    {
        Cursor.lockState = CursorLockMode.Locked;
        menuUI.SetActive(false);
        isPaused = false;
        GameEvents.Instance.unlockCamera();
        GameEvents.Instance.playerCanMove();
        Debug.Log("Game is Un - Paused!");
    }
    //This unlocks the cursor and brings up the pause UI.
    public void Pause()
    {
        Cursor.lockState = CursorLockMode.None;
        menuUI.SetActive(true);
        isPaused = true;
        GameEvents.Instance.lockCamera();
        GameEvents.Instance.playerCanMove();
        Debug.Log("Game is Paused!");
    }

    public void OptionsOn()
    {
        optionsUI.SetActive(true);
    }
    public void OptionsOff()
    {
        optionsUI.SetActive(false);
    }
}
