﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[SerializeField]
public class SceneChanger : MonoBehaviour {

    public string sceneName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SwitchTo()
    {
        Debug.Log("Player Switched Scenes...");
        SceneManager.LoadScene(sceneName);
    }

    public void Exit()
    {
        Debug.Log("Player has quit...");
        Application.Quit();
    }

}
