﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour {

    public float scrollX;
    public float scrollY;

    //public bool forward;

    private void Update()
    {

      //  if (forward) {
            float OffsetX = Time.time * scrollX;
            float OffsetY = Time.time * scrollY;
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(OffsetX, OffsetY);

        //    Invoke("MakeNotForward", 1);

        //}
        //else if (!forward)
        //{
        //    float OffsetX = - Time.time * scrollX;
        //    float OffsetY = - Time.time * scrollY;
        //    GetComponent<Renderer>().material.mainTextureOffset = new Vector2(OffsetX, OffsetY);

        //    Invoke("MakeForward", 1);

        //}
    }

    //void MakeForward()
    //{
    //    forward = true;
    //}
    //void MakeNotForward()
    //{
    //    forward = false;
    //}
}
