﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCamera : MonoBehaviour {

    GameObject player;
    
    //Mouse movement.
    Vector2 mouseLook;
    Vector2 smoothVector;
    
    //Mouse Sensitivity.
    public float sensitivity;
    private float sensBeforeLock;
    float maximumx = -90f;
    float minimumx = 80f;

    //This is to smooth/lerp the camera.
    public float smoothing;

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {
        //Just setting the player to the Camera.
        player = transform.parent.gameObject;

        GameEvents.Instance.lockCamera += HandleLockCamera;
        GameEvents.Instance.unlockCamera += HandleUnlockCamera;

    }
	
	// Update is called once per frame
	void Update () {

            // md = mouse delta.
            //This line is checking the movement of the mouse.
            var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            //This chunk is for smoothing the look movement. It normalizes it and lerps the movement smoothly.
            md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
            smoothVector.x = Mathf.Lerp(smoothVector.x, md.x, 1f / smoothing);
            smoothVector.y = Mathf.Lerp(smoothVector.y, md.y, 1f / smoothing);
            mouseLook += smoothVector;

            mouseLook.y = Mathf.Clamp(mouseLook.y, -80, 90);
            transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
            player.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, player.transform.up);
    }

    private void HandleLockCamera()
    {
        sensBeforeLock = sensitivity;
        sensitivity = 0;
    }

    private void HandleUnlockCamera()
    {
        sensitivity = sensBeforeLock;
    }
}
