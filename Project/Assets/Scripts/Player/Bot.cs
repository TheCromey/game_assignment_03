﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : MonoBehaviour {

    NavMeshAgent agent;

    public Ship ship;

    //public GameObject botPanel;
    public GameObject player;

    public GameObject anchorWaypoint;
    public GameObject cannonWaypoint;

    public bool beingHit;
    public bool selected;
    public bool onAnchor;
    public bool onCannon;
    public bool reachedCannon;

	// Use this for initialization
	void Start () {

        agent = GetComponent<NavMeshAgent>();        
	}
	
	// Update is called once per frame
	void Update () {

        if(player.GetComponent<PlayerController>().hittingBot == false)
        {
            beingHit = false;
        }       

        if (selected)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else if (!selected && beingHit)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.blue;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.white;
        }
         
        if (onAnchor && agent.enabled)
        {
            agent.SetDestination(anchorWaypoint.transform.position);
            reachedCannon = false;

            if (agent.remainingDistance < 0.5f)
            {
                agent.enabled = false;
                UseAnchor();
            }
        }

        if(onCannon && agent.enabled)
        {
            agent.SetDestination(cannonWaypoint.transform.position);
            reachedCannon = false;

            if (agent.remainingDistance < 0.5f)
            {
                agent.enabled = false;
                reachedCannon = true;
                onCannon = false;
            }
        }
	}

    public void UseAnchor()
    {
        if (ship.isAnchored == true)
        {
            ship.isAnchored = false;
        }
        else
        {
            ship.isAnchored = true;
        }
        GameEvents.Instance.botUsedAnchor();
        onAnchor = false;
    }

}
