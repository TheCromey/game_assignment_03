﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour {

    private static GameEvents instance;
    public static GameEvents Instance
    {
        get
        {
            return instance ?? (instance = new GameObject("GameEvents").AddComponent<GameEvents>());
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);    
    }

    public void Initialize()
    {

    }

    public delegate void PlayerDied();
    public PlayerDied playerDied;

    public delegate void DummyDied();
    public DummyDied dummyDied;

    public delegate void PlayerUnMounting();
    public PlayerUnMounting playerUnMounting;

    public delegate void LockCamera();
    public LockCamera lockCamera;

    public delegate void UnlockCamera();
    public UnlockCamera unlockCamera;

    public delegate void PlayerCanMove();
    public PlayerCanMove playerCanMove;

    //This is for handling the weapons Exit functions.
    public delegate void WeaponExit();
    public WeaponExit weaponExit;

    public delegate void PlayerOnCannon();
    public PlayerOnCannon playerOnCannon;

    public delegate void BotUsedAnchor();
    public BotUsedAnchor botUsedAnchor;
}
