﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private Transform respawnPoint;
        
    Rigidbody body;

    Animator anim;

    PauseMenu pauseMenu;

    Ray shipCheckRay;

    public GameObject menuUI;

    public Ship ship;
    Cannon cannon;

    public GameObject statsUI;
    public GameObject winUI;
    public GameObject bombPrefab;
    public GameObject hand;

    public GameObject anchorWaypoint;
    public GameObject lCannon1Waypoint;
    public GameObject lCannon2Waypoint;
    public GameObject rCannon1Waypoint;
    public GameObject rCannon2Waypoint;
    public GameObject cannonCamera;

    public GameObject shipBlue;
    public GameObject shipRed;

    public Camera firstP;
    public Camera thirdP;

    public float movementSpeed;
    public float jumpHeight;
    public int throwForce;
    private float startHealthPoints = 100;
    public float currentHealthPoints; 

    public bool alive;
    public bool isGrounded;
    public bool isCannon;
    public bool isAnchor;
    public bool isSteeringWheel;
    public bool canMove;
    public bool onCannon;
    public bool drivingShip;
    public bool isBoardingPlank;
    public bool hittingBot;

    public Image healthBar;

    List<GameObject> weapons = new List<GameObject>();
    public List<GameObject> selectedBots = new List<GameObject>();

    private void Awake()
    {
        //This locks the mouse to the screen and turn it off.
        Cursor.lockState = CursorLockMode.Locked;
        alive = true;
        canMove = true;
    }

    // Use this for initialization
    void Start()
    {

        AssignTeam();

        pauseMenu = GetComponent<PauseMenu>();

        GameEvents.Instance.Initialize();

        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        shipBlue = GameObject.Find("Ship_Blue");
        shipRed = GameObject.Find("Ship_Red");
        cannon = GetComponent<Cannon>();

        GameEvents.Instance.playerDied += HandlePlayerDied;
        GameEvents.Instance.playerUnMounting += HandlePlayerUnMounting;
        GameEvents.Instance.playerCanMove += HandlePlayerCanMove;

        currentHealthPoints = startHealthPoints;
    }

    // Update is called once per frame
    void Update()
    {

        if (alive && canMove)
        {
            //Using the Unity Inputs this checks which direction the player is moving and multiplies it by the movementspeed.
            float forBack = Input.GetAxis("Vertical") * movementSpeed;
            float rightLeft = Input.GetAxis("Horizontal") * movementSpeed;
            
            //This will make the player move more smoothly.
            forBack *= Time.deltaTime;
            rightLeft *= Time.deltaTime;

            //This then pushes the player in the direction pressed.
            transform.Translate(rightLeft, 0, forBack);

            //This will make the player jump up.
            if (Input.GetButtonDown("Jump") && isGrounded == true)
            {
                body.AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Impulse);
                isGrounded = false;
            }
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
            {
                anim.SetInteger("Walk", 1);
            }else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
            {
                anim.SetInteger("Walk", 0);
            }
            //This checks if the player is mounting. If yes then switch camera to TP.
            if (Input.GetKeyDown(KeyCode.E))
            {
              
                if (isCannon) {
                    onCannon = true;
                    isCannon = false;
                    canMove = false;
                    firstP.gameObject.SetActive(false);
                }
                if (isAnchor)
                {
                    if (ship.isAnchored == true)
                    {
                        ship.isAnchored = false;
                    }
                    else
                    {
                        ship.isAnchored = true;
                    }
                }

                if (isSteeringWheel)
                {
                    drivingShip = true;
                    ship.steering = true;
                    isSteeringWheel = false;
                    canMove = false;
                }

            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                ThrowBomb();
            }

        }

        //This unlocks the cursor and hows it again when the player hits escape.
        
        if (body.velocity.y < 0f)
            body.velocity = new Vector2(0, body.velocity.y * 1.04f);

        //Switches from FP Camera to TP Camera on Death.
        if (!alive)
        {
            SwitchCameraThirdPerson();
            Invoke("Respawn", 3);

            if (currentHealthPoints >= 0)
            {
                GameEvents.Instance.playerDied();
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            statsUI.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            statsUI.SetActive(false);
        }
       

    }

    private void FixedUpdate() {

        RaycastHit hit;
        //These are for when the player is on either ship they will move along with it.
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit)) {

            if (hit.collider.gameObject.tag == "Ship_Blue")
            {
                //transform.parent = shipObject.transform;
                transform.parent = shipBlue.transform;
                ship = shipBlue.GetComponent<Ship>();
            }
            else if(hit.collider.gameObject.tag == "Ship_Red")
            {
                //transform.parent = boat.transform;
                transform.parent = shipRed.transform;
                ship = shipRed.GetComponent<Ship>();
            }
        }

        if (Physics.Raycast(transform.position, firstP.transform.forward, out hit))
        {
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.tag == "Bot_Blue" && tag == "Player_Blue")
                {
                    hit.collider.gameObject.GetComponent<Bot>().beingHit = true;
                    hittingBot = true;
                    if (Input.GetMouseButtonDown(1))
                    {
                        if (hit.collider.gameObject.GetComponent<Bot>().selected == false)
                        {
                            hit.collider.gameObject.GetComponent<Bot>().selected = true;
                            selectedBots.Add(hit.collider.gameObject);
                        }
                        else
                        {
                            hit.collider.gameObject.GetComponent<Bot>().selected = false;
                            selectedBots.Remove(hit.collider.gameObject);
                        }
                    }
                }
                else if (hit.collider.gameObject.tag == "Bot_Red" && tag == "Player_Red")
                {
                    hit.collider.gameObject.GetComponent<Bot>().beingHit = true;
                    hittingBot = true;
                    if (Input.GetMouseButtonDown(1))
                    {
                        if (hit.collider.gameObject.GetComponent<Bot>().selected == false)
                        {
                            hit.collider.gameObject.GetComponent<Bot>().selected = true;
                            selectedBots.Add(hit.collider.gameObject);
                        }
                        else
                        {
                            hit.collider.gameObject.GetComponent<Bot>().selected = false;
                            selectedBots.Remove(hit.collider.gameObject);
                        }
                    }
                }
                else
                {
                    hittingBot = false;
                }
            }

            else
            {
                if (hit.collider == null)
                {
                    hittingBot = false;
                }
            }
        }
        if (Physics.Raycast(transform.position, firstP.transform.forward, out hit))
        {

            if (hit.collider.gameObject.tag == "Anchor")
            {
                if (selectedBots != null && Input.GetKeyDown(KeyCode.F))
                {
                    foreach (GameObject bot in selectedBots)
                    {
                        bot.GetComponent<NavMeshAgent>().enabled = true;
                        bot.GetComponent<NavMeshAgent>().SetDestination(anchorWaypoint.transform.position);
                        bot.GetComponent<Bot>().onAnchor = true;
                    }
                    ClearBotList();
                }
            }

            if (hit.collider.gameObject.tag == "Cannon")
            {
                if (selectedBots != null && Input.GetKeyDown(KeyCode.F))
                {
                    foreach (GameObject bot in selectedBots)
                    {
                        bot.GetComponent<NavMeshAgent>().enabled = true;
                        if(hit.collider.gameObject.name == "Left_Cannon_Holder_1")
                        {
                            bot.GetComponent<NavMeshAgent>().SetDestination(lCannon1Waypoint.transform.position);
                            bot.GetComponent<Bot>().cannonWaypoint = lCannon1Waypoint;
                        }
                        if (hit.collider.gameObject.name == "Left_Cannon_Holder_2")
                        {
                            bot.GetComponent<NavMeshAgent>().SetDestination(lCannon2Waypoint.transform.position);
                            bot.GetComponent<Bot>().cannonWaypoint = lCannon2Waypoint;
                        }
                        if (hit.collider.gameObject.name == "Right_Cannon_Holder_1")
                        {
                            bot.GetComponent<NavMeshAgent>().SetDestination(rCannon1Waypoint.transform.position);
                            bot.GetComponent<Bot>().cannonWaypoint = rCannon1Waypoint;
                        }
                        if (hit.collider.gameObject.name == "Right_Cannon_Holder_2")
                        {
                            bot.GetComponent<NavMeshAgent>().SetDestination(rCannon2Waypoint.transform.position);
                            bot.GetComponent<Bot>().cannonWaypoint = rCannon2Waypoint;
                        }

                        bot.GetComponent<Bot>().onCannon = true;
                    }
                    ClearBotList();
                }
            }
        }

            Debug.DrawRay(transform.position, firstP.transform.forward);
    }

    private void OnCollisionEnter(Collision collision) {

        //Checks if the player is on the ground.
        if (collision.gameObject.tag == "Ship_Blue" || collision.gameObject.tag == "Ship_Red")
            isGrounded = true;

        //If a deadly object hits the player it kills them.
        if (collision.gameObject.tag == "Cannon_Ball") {
           HandlePlayerDied();
        }
        if (collision.gameObject.tag == "Sword")
        {
            currentHealthPoints -= 30f;
            TakingDamage(.3f);
            if (currentHealthPoints <= 0)
            {
                HandlePlayerDied();
            }
        }
        if (collision.gameObject.tag == "Bullet")
        {
            currentHealthPoints -= 70f;
            TakingDamage(.7f);
            if (currentHealthPoints <= 0)
            {
                HandlePlayerDied();
            }
        }

    }


    //This switches from the FPCamera to the TPCamera.
    void SwitchCameraThirdPerson()
    {
        firstP.gameObject.SetActive(false);
        thirdP.gameObject.SetActive(true);
    }
    
    //This switches from the TPCamera to the FPCamera.
    void SwitchCameraFirstPerson()
    {
        firstP.gameObject.SetActive(true);
        thirdP.gameObject.SetActive(false);
    }
    
    //This teleports the players position back to the spawn point and resets all of the contraints to stop any spinning/motion, etc... 
    public void Respawn()
    {
        body.constraints = RigidbodyConstraints.FreezeAll;
        transform.position = respawnPoint.transform.position;
        body.constraints = RigidbodyConstraints.None;
        alive = true;
        body.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        SwitchCameraFirstPerson();
        currentHealthPoints = startHealthPoints;
        TakingDamage(1f);
    }

    public void TakingDamage(float amount)
    {
        healthBar.fillAmount = currentHealthPoints / startHealthPoints;
    }

    //This is a delegate Handler for the player's death.
    private void HandlePlayerDied()
    {

        Debug.Log("Player Died!!");
        if (onCannon)
        {
            onCannon = false;
            cannonCamera.gameObject.SetActive(false);
            firstP.gameObject.SetActive(true);
        }
        alive = false;
        currentHealthPoints = 0;
        TakingDamage(0f);
        
    }

    private void HandlePlayerCanMove()
    {
        if (PauseMenu.isPaused == true)
        {
            canMove = false;
        }
        else if (PauseMenu.isPaused == false)
        {
            canMove = true;
        }
    }

    //This is a delegate handler for the player unmounting.
    private void HandlePlayerUnMounting()
    {
        onCannon = false;
        firstP.gameObject.SetActive(true);
        ship.steering = false;
        canMove = true;
        drivingShip = false;
        cannonCamera.gameObject.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        //Checks to see if the player can mount a vehicle/mount/item.
        if (gameObject.tag == "Player_Blue")
        {
            if (other.gameObject.tag == "Cannons_Blue")
                isCannon = true;

            if (other.gameObject.tag == "Anchor_Blue")
                isAnchor = true;

            if (other.gameObject.tag == "Helm_Blue")
                isSteeringWheel = true;

            if (other.gameObject.tag == "BoardinPlank_Blue")
                isBoardingPlank = true;
        }
        //if (other.gameObject.tag == "Cannon")
        //    isCannon = true;
        
        //if (other.gameObject.tag == "Anchor")
        //    isAnchor = true;
        
        if (other.gameObject.tag == "Deadly")
            alive = false;

        //if (other.gameObject.tag == "Ship_Wheel")
        //    isSteeringWheel = true;

        //if (other.gameObject.tag == "Boarding_Plank")
        //    isBoardingPlank = true;

        //if (other.gameObject.tag == "GOLD")
        //{
        //    winUI.SetActive(true);
        //    Invoke("Menu", 10);
        //}
    }
    public void OnTriggerExit(Collider other)
    {
        //This is for unmounting the ships components.
        if (gameObject.tag == "Player_Blue")
        {
            if (other.gameObject.tag == "Cannons_Blue")
            {
                isCannon = false;
                onCannon = false;
                transform.parent = ship.gameObject.transform;
            }

            if (other.gameObject.tag == "Anchor_Blue")
            {
                isAnchor = false;
            }

            if (other.gameObject.tag == "Helm_Blue")
            {
                isSteeringWheel = false;
            }

            if (other.gameObject.tag == "BoardinPlank_Blue")
            {
                isBoardingPlank = false;
            }
        }

        //if (other.gameObject.tag == "Cannon") {

        //    isCannon = false;
        //    onCannon = false;
        //    transform.parent = ship.gameObject.transform;
        //}

        //if (other.gameObject.tag == "Anchor") {

        //    isAnchor = false;

        //}

        if (other.gameObject.tag == "Ship_Wheel")
            isSteeringWheel = false;

        if (other.gameObject.tag == "Boarding_Plank")
            isBoardingPlank = false;
    }

    void Menu()
    {
        SceneManager.LoadScene("Main_Menu");
    }

    public void ThrowBomb()
    {
        GameObject bomb = GameObject.Instantiate(bombPrefab, hand.transform.position, hand.transform.rotation);
        bomb.GetComponent<Rigidbody>().AddForce(hand.transform.forward * throwForce);
    }

    public void ClearBotList()
    {
        foreach(GameObject bot in selectedBots)
        {
            bot.GetComponent<Bot>().selected = false;
            selectedBots.Remove(bot);
        }
    }

    public void AssignTeam()
    {
        RaycastHit hitShip;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hitShip))
        {

            if (hitShip.collider.gameObject.tag == "Ship_Blue")
            {
                AssignBlueTeam();
            }
            else if (hitShip.collider.gameObject.tag == "Ship_Red")
            {
                AssignRedTeam();
            }

        }

    }

    public void AssignBlueTeam()
    {
        transform.gameObject.tag = "Player_Blue";
        respawnPoint = GameObject.FindWithTag("RespawnPoint_Blue").transform;
        anchorWaypoint = GameObject.FindGameObjectWithTag("Anchor_Waypoint_Blue");
        lCannon1Waypoint = GameObject.FindGameObjectWithTag("LC_01_Waypoint_Blue");
        lCannon2Waypoint = GameObject.FindGameObjectWithTag("LC_02_Waypoint_Blue");
        rCannon1Waypoint = GameObject.FindGameObjectWithTag("RC_01_Waypoint_Blue");
        rCannon2Waypoint = GameObject.FindGameObjectWithTag("RC_02_Waypoint_Blue");
        Debug.Log("I am Blue Team!!");
    }

    public void AssignRedTeam()
    {
        transform.gameObject.tag = "Player_Red";
        respawnPoint = GameObject.FindWithTag("RespawnPoint_Red").transform;
        anchorWaypoint = GameObject.FindGameObjectWithTag("Anchor_Waypoint_Red");
        lCannon1Waypoint = GameObject.FindGameObjectWithTag("LC_01_Waypoint_Red");
        lCannon2Waypoint = GameObject.FindGameObjectWithTag("LC_02_Waypoint_Red");
        rCannon1Waypoint = GameObject.FindGameObjectWithTag("RC_01_Waypoint_Red");
        rCannon2Waypoint = GameObject.FindGameObjectWithTag("RC_02_Waypoint_Red");
        Debug.Log("I am Red Team!!");
    }
}
