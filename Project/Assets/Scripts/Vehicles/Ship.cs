﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

    Rigidbody body;

    PlayerController player;

    public GameObject helm;

    public int healthPoints;
    public int planks;

    public float defaultSpeed;
    public float curSpeed;
    public float turnSpeed;

    public bool isAnchored;
    public bool steering;

	// Use this for initialization
	void Start () {

        body = GetComponent<Rigidbody>();

        isAnchored = true;

        player = GetComponent<PlayerController>();

	}
	
	// Update is called once per frame
	void Update () {

        if (isAnchored == false)
        {
            curSpeed = defaultSpeed;
            Moving();
        }else if (healthPoints <= 0)
        {
            Debug.Log("Ship has been damaged and can no longer move!!");
            StopMoving();
        }
        if (steering)
        {

            if (curSpeed >= 1)
            {
                if (Input.GetKey(KeyCode.A)) {
                    transform.Rotate(Vector3.down * turnSpeed * Time.deltaTime);
                    if (helm.transform.rotation.eulerAngles.z >= 30) {
                        helm.transform.Rotate(-Vector3.forward * 1);
                    }

                }
                if (Input.GetKey(KeyCode.D)) {
                    transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime);
                    if (helm.transform.rotation.eulerAngles.z <= 70) {
                        helm.transform.Rotate(Vector3.forward * 1);
                    }

                }
            }
            if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Space))
            {
                GameEvents.Instance.playerUnMounting();
                player.isSteeringWheel = true;
            }
        }
        else if(isAnchored)
        {
            curSpeed = 0;
        }

	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Cannon_Ball")
        {
            healthPoints -= 10;
        }
    }

    //Ship Moves
    public void Moving()
    {
        transform.position += transform.forward * curSpeed * Time.deltaTime;
    }
    public void StopMoving()
    {
        transform.position += transform.forward * 0;
    }
}
