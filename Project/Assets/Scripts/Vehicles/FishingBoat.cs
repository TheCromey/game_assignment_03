﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingBoat : MonoBehaviour {

    Rigidbody body;

    public int healthPoints;

    public float defaultSpeed;
    public float curSpeed;

    public bool imAnchored;

    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody>();
        Invoke("LiftAnchor", 5);
    }

    // Update is called once per frame
    void Update()
    {
       
        if (imAnchored == false)
        {
            curSpeed = defaultSpeed;
            Moving();
        }
        else if (imAnchored)
        {
            curSpeed = 0;
        }

    }
    //Ship Moves
    public void Moving()
    {

        transform.position += transform.forward * curSpeed * Time.deltaTime;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Deadly")
        {
            imAnchored = true;
        }
    }

    void LiftAnchor()
    {
        imAnchored = false;
    }

}