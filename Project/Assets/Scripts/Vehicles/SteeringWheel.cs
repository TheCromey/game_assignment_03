﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheel : MonoBehaviour {

    Ship ship;

    bool canUse;

	// Use this for initialization
	void Start () {
        GameObject shipObject = GameObject.FindGameObjectWithTag("Ship");
        ship = shipObject.GetComponent<Ship>();
    }
	
	// Update is called once per frame
	void Update () {
        if (canUse) {
            if (Input.GetKeyDown(KeyCode.E))
            {
                ship.steering = true;
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
                canUse = true;            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            canUse = false;
        }
    }
}
