﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapStanAnimator : MonoBehaviour {

    Ship ship;
   

    public Animator animator;
    public bool canUse;

    // Use this for initialization
    void Start()
    {

        //GameObject shipObject = GameObject.FindGameObjectWithTag("Ship");
        //shipBlue = GetComponent<Ship>();
        ship = GetComponent<Ship>();
       

        animator.SetInteger("Drop", 1);
        Invoke("ResetAnim", .5f);

        GameEvents.Instance.botUsedAnchor += HandleBotUsedAnchor;

    }

    // Update is called once per frame
    void Update()
    {
        if (canUse)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (ship.isAnchored == false)
                {
                    animator.SetInteger("Raise", 1);
                    Invoke("ResetAnim", .5f);
                }
                else if (ship.isAnchored == true)
                {
                    animator.SetInteger("Drop", 1);
                    Invoke("ResetAnim", .5f);
                }
            }
        }
    }
    //Resets the parametres for the animator.
    private void ResetAnim()
    {
        animator.SetInteger("Raise", 0);
        animator.SetInteger("Drop", 0);
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player_Blue" || other.gameObject.tag == "Player_Red")
        {
            canUse = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player_Blue" || other.gameObject.tag == "Player_Red")
        {
            canUse = false;
        }
    }

    private void HandleBotUsedAnchor()
    {
        if (ship.isAnchored == false)
        {
            animator.SetInteger("Raise", 1);
            Invoke("ResetAnim", .5f);
        }
        else if (ship.isAnchored == true)
        {
            animator.SetInteger("Drop", 1);
            Invoke("ResetAnim", .5f);
        }
    }
}
