﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardingPlank : MonoBehaviour {

    public bool canUse;

    public Animator anim;

    bool raised = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (canUse)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (raised)
                {
                    Drop();
                }else if (!raised)
                {
                    Raise();
                }
            }
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player_Blue" || other.gameObject.tag == "Player_Red")
        {
            canUse = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player_Blue" || other.gameObject.tag == "Player_Red")
        {
            canUse = false;
        }
    }

    void ResetAnim()
    {
        anim.SetInteger("Drop", 0);
        anim.SetInteger("Raise", 0);
    }

    void Drop()
    {
        anim.SetInteger("Drop", 1);
        raised = false;
        Invoke("ResetAnim", 1);
    }
    void Raise()
    {
        anim.SetInteger("Raise", 1);
        raised = true;
        Invoke("ResetAnim", 1);
    }
}
