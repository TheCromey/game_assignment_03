﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorAnimator : MonoBehaviour {

    public Ship ship;

    public Animator animator;
	// Use this for initialization
	void Start () {

        animator.SetInteger("Drop", 1);

    }
	
	// Update is called once per frame
	void Update () {

        if (ship.isAnchored == false)
        {
            animator.SetInteger("Raise", 1);
            animator.SetInteger("Drop", 0);
        }
        else if(ship.isAnchored == true)
        {
            animator.SetInteger("Raise", 0);
            animator.SetInteger("Drop", 1);
        }
    }
}
