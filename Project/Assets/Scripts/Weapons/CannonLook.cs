﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonLook : MonoBehaviour {

    public GameObject cannon;
    public GameObject barrel;
    public GameObject barrelHinge;
    //public GameObject player;

    void Start() {

    }

    // Update is called once per frame
    void Update() {

        if (gameObject.tag == "Cannon_Right")
        {
            Vector3 currentRotation = barrelHinge.transform.localRotation.eulerAngles;            

            if (Input.GetKey(KeyCode.W))
            {
                currentRotation.x += 10 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                currentRotation.x -= 10 * Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.D))
            {
                if (cannon.transform.localRotation.y <= -0.46)
                {
                    cannon.transform.Rotate(Vector3.forward);
                }
            }
            if (Input.GetKey(KeyCode.A))
            {
                if (cannon.transform.localRotation.y >= -0.54)
                {
                    cannon.transform.Rotate(-Vector3.forward);
                }
            }
        }
        if (gameObject.tag == "Cannon_Left")
        {
            Vector3 currentRotation = barrelHinge.transform.localRotation.eulerAngles;

            if (Input.GetKey(KeyCode.W))
            {
                currentRotation.x += 10 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                currentRotation.x -= 10 * Time.deltaTime;
            }

            barrelHinge.transform.localRotation = Quaternion.Euler(currentRotation);

            if (Input.GetKey(KeyCode.D))
            {
                if (cannon.transform.localRotation.y <= 0.54)
                {
                    cannon.transform.Rotate(Vector3.forward);
                }
            }

            if (Input.GetKey(KeyCode.A))
            {
                if (cannon.transform.localRotation.y >= 0.46)
                {
                    cannon.transform.Rotate(-Vector3.forward);
                }
            }
        }

    }
}