﻿using UnityEngine;
using UnityEngine.Networking;

public class Pistol : MonoBehaviour {

    public int force;
    public float range;

    public GameObject muzzel;
    public GameObject bulletPrefab;

    public Animator anim;

    public float reloadTime;
    float countDown;

    [SerializeField]
    private LayerMask mask;

	// Use this for initialization
	void Start () {
        GameEvents.Instance.weaponExit += HandleWeaponExit;
	}
	
	// Update is called once per frame
	void Update () {

        if (countDown >= 0)
            countDown -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0) && countDown <= 0)
            FireAnimation();
    }

    void Fire()
    {

        //RaycastHit hit;

        //if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, range, mask))
        //{
        //    if (hit.collider != null)
        //    {
        //        Debug.Log("I hit something!!");
                
        //        if (hit.collider.gameObject.tag == "Player_Blue" || hit.collider.gameObject.tag == "Player_Red")
        //        {

        //        }
        //    }
        //    else if (hit.collider == null)
        //    {

        //    }
        //}

        GameObject bullet = GameObject.Instantiate(bulletPrefab, muzzel.transform.position, muzzel.transform.rotation);
        bullet.GetComponent<Rigidbody>().AddForce(- muzzel.transform.forward * -force);

        countDown = reloadTime;

        Invoke("EndFire", .5f);

    }

    void FireAnimation()
    {
        anim.SetInteger("Fire_001", 1);
        Invoke("Fire", .15f);
    }

    void EndFire()
    {
        anim.SetInteger("Fire_001", 0);
    }

    void HandleWeaponExit()
    {
        anim.SetInteger("Exit", 1);
    }
}
