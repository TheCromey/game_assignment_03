﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {

    public Animator anim;
    public float ready = 1;
    WeaponManager weaponManager;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();

        GameEvents.Instance.weaponExit += HandleWeaponExit;
        weaponManager = GetComponent<WeaponManager>();
     
	}
	
	// Update is called once per frame
	void Update () {

        if (ready >= 0)
            ready -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            Attack();
        }

    }

    public void AttackEnd()
    {
        anim.SetInteger("Attack", 0);
    }

    void Attack()
    {
        if (ready <= 0)
        {
            anim.SetInteger("Attack", 1);
            Invoke("AttackEnd", 1);
            ready = 1;
        }else if (ready >= 0)
        {
            anim.SetInteger("Attack", 2);
        }
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.tag == "Bot")
                {

                }else if (hit.collider.gameObject.tag == "Player")
                {

                }else if (hit.collider.gameObject.tag == "Ship")
                {

                }else if (hit.collider == null)
                {

                }
            }
        }
    }


    void HandleWeaponExit()
    { 
        anim.SetInteger("Exit", 1);
    }

    public void WeaponExit()
    {
        weaponManager.SwitchWeapon();
    }
}
