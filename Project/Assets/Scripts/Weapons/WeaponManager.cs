﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour {

    public int currentWeapon = 0;

	// Use this for initialization
	void Start () {
        SwitchWeapon();
    }
	
	// Update is called once per frame
	void Update () {


	}

    private void FixedUpdate()
    {
        int lastSelectedWeapon = currentWeapon;

        //This checks which weapon the player is on when they use the scroll wheel.
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (currentWeapon >= transform.childCount - 1)
                currentWeapon = 0;
            else
                currentWeapon++;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (currentWeapon <= 0)
                currentWeapon = transform.childCount - 1;
            else
                currentWeapon--;
        }


        //This then changes the weapon and calls the active weapons "Exit" behaviours.
        if (lastSelectedWeapon != currentWeapon)
        {
            Invoke("SwitchWeapon", .4f);
            GameEvents.Instance.weaponExit();
        }
    }

    //This checks what weapon the player is switching to and de-activates the last one.
    public void SwitchWeapon()
    {

        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == currentWeapon)
                weapon.gameObject.SetActive(true);
            else
                weapon.gameObject.SetActive(false);
            i++;
        }
    }

}
