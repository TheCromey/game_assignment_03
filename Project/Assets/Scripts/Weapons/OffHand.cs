﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffHand : MonoBehaviour
{

    public GameObject grenadePrefab;
    public int force;
    public GameObject hand;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }

    }

    void Fire()
    {
        GameObject grenade = GameObject.Instantiate(grenadePrefab, hand.transform.position, hand.transform.rotation);
        grenade.GetComponent<Rigidbody>().AddForce(transform.forward * force);

    }
    public void Explode()
    {

    }

}
