﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour {

    float countdown;
    public float delay = 2;
    public int force;
    public int radius;

    public Animator anim;

    // Use this for initialization
    void Start () {
        countdown = delay;
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f)
            Explode();
    }

  
    public void Explode()
    {
       Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody body = nearbyObject.GetComponent<Rigidbody>();
            if (body != null)
            {
                foreach (Collider collider in colliders)
                {
                    if(collider.gameObject.tag == "Player")
                    {
                        GameEvents.Instance.playerDied();
                    }
                 
                }
                    body.AddExplosionForce(force, transform.position, radius);
                
            }
        }
        anim.SetInteger("Explosion", 1);
        Destroy(gameObject, 3);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

}
