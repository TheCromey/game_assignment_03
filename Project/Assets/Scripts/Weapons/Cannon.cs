﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField]
    PlayerController player;

    public Camera cannonCamera;

    public Animator anim;

    public GameObject cBallPrefab;
    public GameObject cannonHole;
    public GameObject raycastPoint;

    RaycastHit hit;

    public bool active;
    public bool botActive;

    public int force;

    public float reloadSpeed;
    float countDown;
    public float lookSensitivity;
    float smoothing = 5f;

    // Use this for initialization
    void Start()
    {

        //player = GetComponent<PlayerController>();
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (countDown >= 0)
        {
            countDown -= Time.deltaTime;
        }

        if (active == true)
        {
            cannonCamera.gameObject.SetActive(true);

            if (Input.GetMouseButtonDown(0) && countDown <= 0)
            {
                Fire();
            }

            if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Space))
            {
                active = false;
                player.isCannon = true;
                GameEvents.Instance.playerUnMounting();
            }
        }
        else
        {
            cannonCamera.gameObject.SetActive(false);
        }

        Debug.DrawRay(raycastPoint.transform.position, raycastPoint.transform.TransformDirection(Vector3.forward), Color.black);
    }

    void Fire()
    {

        GameObject cannonball = GameObject.Instantiate(cBallPrefab, cannonHole.transform.position, cannonHole.transform.rotation);
        cannonball.GetComponent<Rigidbody>().AddForce(cannonHole.transform.up * force);
        countDown = reloadSpeed;
        anim.SetInteger("Fire", 1);
        Invoke("Reload", 1);

    }

    void Reload()
    {
        anim.SetInteger("Fire", 0);
    }

    private void OnTriggerStay(Collider other) {

        if (other.gameObject.tag == "Player_Blue" && player.onCannon == true || other.gameObject.tag == "Player_Red" && player.onCannon == true)
        {
            active = true;
        }

        if (other.gameObject.tag == "Bot")
        {
            botActive = true;
            if (other.gameObject.GetComponent<Bot>().reachedCannon)
            {
                if (countDown <= 0)
                {                    
                    if (Physics.Raycast(raycastPoint.transform.position, raycastPoint.transform.TransformDirection(Vector3.forward), out hit))
                    {
                        if(hit.collider.gameObject.tag == "HitTester")
                        {
                            Fire();
                        }
                    }                    
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            active = false;
            GameEvents.Instance.playerUnMounting();
        }

        if(other.gameObject.tag == "Bot")
        {
            botActive = false;
        }
    }

}