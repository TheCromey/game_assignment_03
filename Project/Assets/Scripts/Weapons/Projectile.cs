﻿using UnityEngine;
using UnityEngine.Networking;

public class Projectile :MonoBehaviour {

    public Animator anim;

    public int damage;

	// Use this for initialization
	void Start () {

    }



	
	// Update is called once per frame
	void Update () {

	}

    private void OnCollisionEnter(Collision collision)
    {
        //if (hasAuthority)
       // {


            //Checks if it has collided with player.
            if (collision.gameObject.tag == "Player")
            {
                CmdPlayerShot(collision.collider.name);
                anim.SetInteger("Collide", 1);
                Destroy(gameObject, .5f);
            }
            else
            {
                HitSomethingElse();
            }

       // }
    }
   // [Command]
    void CmdPlayerShot(string _ID)
    {
        Debug.Log(_ID + "has been HIT");
    }

   // [Client]
    void HitSomethingElse()
    {
        Debug.Log("I HIT SOMETHING");
        anim.SetInteger("Collide", 1);
        Destroy(gameObject, .5f);
    }

}
